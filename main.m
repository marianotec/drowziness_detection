%%  Drowsiness Detection
% MAIN SCRIPT FOR DROWSINESS DETECTION

% ------------------------------------------------------------------------
%% Workspace Clean-Up

close all;
clear;
clc;

% ------------------------------------------------------------------------
%% Data Loading

load_data

% -----------------------------------------------------------------------
%% Signal Bandwidth Filtering

% Sampling Frequency
fs = 250; % Given by Physionet

ROWS_DIM = 2;

% 0.2Hz to 100Hz Band Pass Filter
dataSetW = filterEEG(dataSetW,ROWS_DIM);
dataSetS = filterEEG(dataSetS,ROWS_DIM);

% Data Backup-Vector
bakDataSetW = dataSetW;
bakDataSetS = dataSetS;

% -----------------------------------------------------------------------
%% Features Extraction

feature_extraction

% -----------------------------------------------------------------------