% -----------------------------------------------------------------------

close all;
clc;

% Which levels are filtered?
% 0: YES
% 1: NO
% wavesDelta      = 0;
% wavesTheta      = 0;
% wavesAlpha      = 0;
% wavesBeta       = 1;
% wavesGamma      = 0;
% wavesHighGamma  = 0;
%
% level = [wavesDelta wavesTheta wavesAlpha wavesBeta wavesGamma wavesHighGamma];

processing

%freqs = 0:(250/2)/(2^(nextpow2(length(avgDataSetW))-1)):(250/2)-1/length(avgDataSetW);

auxFilteredSignal = filterFrom4To8Hz(dataSetW,ROWS_DIM);
powerThetaW = signalPower(auxFilteredSignal,ROWS_DIM);
zerosThetaW = countZeroCrossings(auxFilteredSignal);
meanFreqThetaW = meanfreq(auxFilteredSignal',fs)';
activitySetThetaW = var(auxFilteredSignal,0,ROWS_DIM);
mobilitySetThetaW = var(diff(auxFilteredSignal,1,ROWS_DIM),0,ROWS_DIM);
complexitySetThetaW = sqrt(var(diff(auxFilteredSignal,2,ROWS_DIM),0,ROWS_DIM)./mobilitySetThetaW - activitySetThetaW.^2);

% auxFilteredSignal = filterFrom8To11Hz(avgDataSetW,ROWS_DIM);
% power08To11W = signalPower(auxFilteredSignal,ROWS_DIM);
% zeros08To11W = countZeroCrossings(auxFilteredSignal);
% meanFreq08To11W = meanfreq(auxFilteredSignal',fs)';
% 
% auxFilteredSignal = filterFrom11To16Hz(avgDataSetW,ROWS_DIM);
% power11To16W = signalPower(auxFilteredSignal,ROWS_DIM);
% zeros11To16W = countZeroCrossings(auxFilteredSignal);
% meanFreq11To16W = meanfreq(auxFilteredSignal',fs)';

auxFilteredSignal = filterFrom16To32Hz(dataSetW,ROWS_DIM);
powerBetaW = signalPower(auxFilteredSignal,ROWS_DIM);
zerosBetaW = countZeroCrossings(auxFilteredSignal);
meanFreqBetaW = meanfreq(auxFilteredSignal',fs)';

activitySetBetaW = var(auxFilteredSignal,0,ROWS_DIM);
mobilitySetBetaW = var(diff(auxFilteredSignal,1,ROWS_DIM),0,ROWS_DIM);
complexitySetBetaW = sqrt(var(diff(auxFilteredSignal,2,ROWS_DIM),0,ROWS_DIM)./mobilitySetBetaW - activitySetBetaW.^2);



auxFilteredSignal = filterFrom4To8Hz(dataSetS,ROWS_DIM);
powerThetaS = signalPower(auxFilteredSignal,ROWS_DIM);
zerosThetaS = countZeroCrossings(auxFilteredSignal);
meanFreqThetaS = meanfreq(auxFilteredSignal',fs)';

activitySetThetaS = var(auxFilteredSignal,0,ROWS_DIM);
mobilitySetThetaS = var(diff(auxFilteredSignal,1,ROWS_DIM),0,ROWS_DIM);
complexitySetThetaS = sqrt(var(diff(auxFilteredSignal,2,ROWS_DIM),0,ROWS_DIM)./mobilitySetThetaS - activitySetThetaS.^2);

% auxFilteredSignal = filterFrom8To11Hz(avgDataSetS,ROWS_DIM);
% power08To11S = signalPower(auxFilteredSignal,ROWS_DIM);
% zeros08To11S = countZeroCrossings(auxFilteredSignal);
% meanFreq08To11S = meanfreq(auxFilteredSignal',fs)';
% 
% auxFilteredSignal = filterFrom11To16Hz(avgDataSetS,ROWS_DIM);
% power11To16S = signalPower(auxFilteredSignal,ROWS_DIM);
% zeros11To16S = countZeroCrossings(auxFilteredSignal);
% meanFreq11To16S = meanfreq(auxFilteredSignal',fs)';

auxFilteredSignal = filterFrom16To32Hz(dataSetS,ROWS_DIM);
powerBetaS = signalPower(auxFilteredSignal,ROWS_DIM);
zerosBetaS = countZeroCrossings(auxFilteredSignal);
%tic
meanFreqBetaS = meanfreq(auxFilteredSignal',fs)';
%toc
% medianFreq value takes more computational time and results are equal
%tic
%medFreqBetaS = meanfreq(auxFilteredSignal,fs);
%toc
activitySetBetaS = var(auxFilteredSignal,0,ROWS_DIM);
mobilitySetBetaS = var(diff(auxFilteredSignal,1,ROWS_DIM),0,ROWS_DIM);
complexitySetBetaS = sqrt(var(diff(auxFilteredSignal,2,ROWS_DIM),0,ROWS_DIM)./mobilitySetBetaS - activitySetBetaS.^2);



auxFilteredSignal = filterAlphaBand(dataSetW,ROWS_DIM);
powerAlphaW = signalPower(auxFilteredSignal,ROWS_DIM);
zerosAlphaW = countZeroCrossings(auxFilteredSignal);
meanFreqAlphaW = meanfreq(auxFilteredSignal',fs)';

activitySetAlphaW = var(auxFilteredSignal,0,ROWS_DIM);
mobilitySetAlphaW = var(diff(auxFilteredSignal,1,ROWS_DIM),0,ROWS_DIM);
complexitySetAlphaW = sqrt(var(diff(auxFilteredSignal,2,ROWS_DIM),0,ROWS_DIM)./mobilitySetAlphaW - activitySetAlphaW.^2);



auxFilteredSignal = filterAlphaBand(dataSetS,ROWS_DIM);
powerAlphaS = signalPower(auxFilteredSignal,ROWS_DIM);
zerosAlphaS = countZeroCrossings(auxFilteredSignal);
meanFreqAlphaS = meanfreq(auxFilteredSignal',fs)';

activitySetAlphaS = var(auxFilteredSignal,0,ROWS_DIM);
mobilitySetAlphaS = var(diff(auxFilteredSignal,1,ROWS_DIM),0,ROWS_DIM);
complexitySetAlphaS = sqrt(var(diff(auxFilteredSignal,2,ROWS_DIM),0,ROWS_DIM)./mobilitySetAlphaS - activitySetAlphaS.^2);


activitySetS = var(dataSetS,0,ROWS_DIM);
mobilitySetS = var(diff(dataSetS,1,ROWS_DIM),0,ROWS_DIM);
complexitySetS = sqrt(var(diff(dataSetS,2,ROWS_DIM),0,ROWS_DIM)./mobilitySetS - activitySetS.^2);

activitySetW = var(dataSetW,0,ROWS_DIM);
mobilitySetW = var(diff(dataSetW,1,ROWS_DIM),0,ROWS_DIM);
complexitySetW = sqrt(var(diff(dataSetW,2,ROWS_DIM),0,ROWS_DIM)./mobilitySetW - activitySetW.^2);

%pxxAvgA = pwelch(avgDataSetW,rectwin(4096),[],2^nextpow2(length(avgDataSetW))-1);
%pxxFiltA = pwelch(filteredA,rectwin(4096),[],2^nextpow2(length(filteredA))-1);
%pxxY = pwelch(y,rectwin(4096),[],2^nextpow2(length(y))-1);

%figure();plot(freqs,pxxAvgA);hold on;plot(freqs,pxxY');
