%% Data Loading

% Constant Filename and Filepath Set
filepath='/home/marianok/DataEEG/out_files/';
filenameA='char_compilation_W.csv';
filenameB='char_compilation_1.csv';

% Dynamic Filename and Filepath Set (By GUI)
%[filenameA,filepath] = uigetfile('*');
%.1fataSetB = load(strcat(filepath,filenameB));
%[filenameB,filepath] = uigetfile('*');
%.1fataSetB = load(strcat(filepath,filenameB));

% Datasets Loading
dataSetW = load(strcat(filepath,filenameA));
dataSetS = load(strcat(filepath,filenameB));

% Unused Variables Cleaning
clearvars filepath filenameA filenameB