%% signalPower Function
% THIS FUNCTION CALCULATES THE POWER OF A SIGNAL AS THE SQUARE OF IT'S RMS
% VALUE
%%
function [ powerX ] = signalPower( X , DIM )

    powerX = ( rms(X,DIM).^2 );
    
end