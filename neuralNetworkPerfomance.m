y = myNeuralNetworkFunction(featuresMatrix');

thValue = 0.55;

tp = sum(y(subjectState==1)>thValue)

tn = sum(y(subjectState==0)<thValue)

fn = sum(y(subjectState==1)<thValue)

fp = sum(y(subjectState==0)>thValue)

plot(tn,tp)