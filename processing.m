%% -----------------------------------------------------------------------

avgSetW = signals_average(dataSetW);
avgDataSetW = dataSetW;
powerValueW = signalPower(avgDataSetW,ROWS_DIM);
zerosCrossingsW = countZeroCrossings(avgDataSetW);
meanValueW = mean(dataSetW,ROWS_DIM);

% [CA, LA] = wavedec(avgDataSetA, 5, 'db2');
% auxCA = zeros(1,length(CA));
% inicio = 1;
% for i=1:6
%     if level(i)
%         auxCA(inicio:inicio+LA(i)-1)=CA(inicio:inicio+LA(i)-1);
%     end
%     inicio=inicio+LA(i);
% end
% filteredA = waverec(auxCA,LA,'db2');
% powerFilteredA = signalPower(filteredA);

%% -----------------------------------------------------------------------

avgSetS = signals_average(dataSetS);
powerValueS = signalPower(dataSetS,ROWS_DIM);
zerosCrossingsS = countZeroCrossings(dataSetS);
meanValueS = mean(dataSetS,ROWS_DIM);

% [CB, LB] = wavedec(avgDataSetB, 5, 'db2');
% auxCB = zeros(1,length(CB));
% inicio = 1;
% for i=1:6
%     if level(i)
%         auxCB(inicio:inicio+LB(i)-1)=CB(inicio:inicio+LB(i)-1);
%     end
%     inicio=inicio+LB(i);
% end
% filteredB = waverec(auxCB,LB,'db2');
% powerFilteredB = signalPower(filteredB);

%%

clearvars i inicio LA LB CA CB auxCA auxCB level