function y = extractAlphaBand(x)
%EXTRACTALPHABAND Filters input x and returns output y.

% MATLAB Code
% Generated by MATLAB(R) 8.5 and the DSP System Toolbox 9.0.
% Generated on: 25-Oct-2016 16:48:30

%#codegen

% To generate C/C++ code from this function use the codegen command.
% Type 'help codegen' for more information.

persistent Hd;

if isempty(Hd)
    
    % The following code was used to design the filter coefficients:
    %
    % Fstop1 = 7.5;   % First Stopband Frequency
    % Fpass1 = 8;     % First Passband Frequency
    % Fpass2 = 15.5;  % Second Passband Frequency
    % Fstop2 = 16;    % Second Stopband Frequency
    % Astop1 = 30;    % First Stopband Attenuation (dB)
    % Apass  = 1;     % Passband Ripple (dB)
    % Astop2 = 30;    % Second Stopband Attenuation (dB)
    % Fs     = 250;   % Sampling Frequency
    %
    % h = fdesign.bandpass('fst1,fp1,fp2,fst2,ast1,ap,ast2', Fstop1, Fpass1, ...
    %                      Fpass2, Fstop2, Astop1, Apass, Astop2, Fs);
    %
    % Hd = design(h, 'cheby2', ...
    %     'MatchExactly', 'passband', ...
    %     'SystemObject', true);
    
    Hd = dsp.BiquadFilter( ...
        'Structure', 'Direct form II', ...
        'SOSMatrix', [1 -1.96249857552703 1 1 -1.83477002072261 ...
        0.986626154046977; 1 -1.83935110652283 1 1 -1.95375172812963 ...
        0.993082929704182; 1 -1.96474383785999 1 1 -1.7995206648446 ...
        0.955088978167726; 1 -1.82946378534151 1 1 -1.94005238983427 ...
        0.977494159540618; 1 -1.96971645753924 1 1 -1.73907152560877 ...
        0.904385070632072; 1 -1.80260654130613 1 1 -1.92154068885311 ...
        0.955377572646127; 1 -1.97840117940988 1 1 -1.62444838569437 ...
        0.804388455465443; 1 -1.72806764960292 1 1 -1.88892791311142 ...
        0.917652413804385; 1 -1.9912014049367 1 1 -1.81751051627884 ...
        0.842089753489105; 1 -1.39088186850032 1 1 -1.43161357039246 ...
        0.611009224807789; 1 0 -1 1 -1.52080735011725 0.582563897672849], ...
        'ScaleValues', [0.924869434521075; 0.924869434521075; ...
        0.902933236057684; 0.902933236057684; 0.857451101344112; ...
        0.857451101344112; 0.757669620008549; 0.757669620008549; ...
        0.526981242636544; 0.526981242636544; 0.208718051163575; 1]);
end

y = step(Hd,x);


