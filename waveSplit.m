function splitedSignal = waveSplit(C,L,signalIndex)

infLimit = 1;
endLimit = L(signalIndex);

if(signalIndex > 1)
    for i = 1:(signalIndex-1)
        infLimit = infLimit + L(i);
    end;
end;

splitedSignal = C(infLimit:(infLimit+endLimit));