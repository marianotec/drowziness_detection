function [ z ] = countZeroCrossings( X )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
z = zeros(1,size(X,1));
for j=1:size(X,1)
    for i=2:size(X,2)
        if( (X(j,i-1)*X(j,i)) < 0 || X(j,i) == 0)
            z(j) = z(j) + 1;
        end
    end
end

z = z';

end

