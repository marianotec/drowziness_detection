%%

% Cleaning of non used variables
clearvars -except bakDataSetS bakDataSetW fs ROWS_DIM

% As multiple time-bases can be used, one must be chosen
basetime_input

%  Datasets are restored from Backup to process multiple times without
% reloading the data
restore_datasets

% -----------------------------------------------------------------------

% The dataset is splitted according to the time-base
signal_time_split

%%% -----------------------------------------------------------------------

eeg

%%% -----------------------------------------------------------------------

%print_values

subjectStateW = ones(size(dataSetW,1),1);
subjectStateS = zeros(size(dataSetS,1),1);

activityFeaturesW = [activitySetThetaW./activitySetW activitySetAlphaW./activitySetW activitySetBetaW./activitySetW activitySetW];
mobilityFeaturesW = [mobilitySetThetaW./mobilitySetW mobilitySetAlphaW./mobilitySetW mobilitySetBetaW./mobilitySetW mobilitySetW];
complexityFeaturesW = [complexitySetThetaW./complexitySetW complexitySetAlphaW./complexitySetW complexitySetBetaW./complexitySetW complexitySetW];
meanFreqFeaturesW = [meanFreqThetaW meanFreqAlphaW meanFreqBetaW];
powerFeaturesW = [powerThetaW./powerBetaW powerAlphaW./powerBetaW powerBetaW./powerValueW powerValueW];

activityFeaturesS = [activitySetThetaS./activitySetS activitySetAlphaS./activitySetS activitySetBetaS./activitySetS activitySetS];
mobilityFeaturesS = [mobilitySetThetaS./mobilitySetS mobilitySetAlphaS./mobilitySetS mobilitySetBetaS./mobilitySetS mobilitySetS];
complexityFeaturesS = [complexitySetThetaS./complexitySetS complexitySetAlphaS./complexitySetS complexitySetBetaS./complexitySetS complexitySetS];
meanFreqFeaturesS = [meanFreqThetaS meanFreqAlphaS meanFreqBetaS];
powerFeaturesS = [powerThetaS./powerBetaS powerAlphaS./powerBetaS powerBetaS./powerValueS powerValueS];

%%% -----------------------------------------------------------------------

featuresW = [activityFeaturesW mobilityFeaturesW complexityFeaturesW meanFreqFeaturesW powerFeaturesW zerosCrossingsW];
featuresS = [activityFeaturesS mobilityFeaturesS complexityFeaturesS meanFreqFeaturesS powerFeaturesS zerosCrossingsS];

featuresMatrix = [featuresW;featuresS];

y = randsample(size(featuresMatrix,1),size(featuresMatrix,1));
subjectState = [subjectStateW;subjectStateS];
z = featuresMatrix(y,:);
sState = subjectState(y,:);

clearvars -except featuresMatrix bakDataSetS bakDataSetW fs ROWS_DIM  avgSetS avgSetW subjectStateW subjectStateS z sState

% -------------------------------------------------------------------------