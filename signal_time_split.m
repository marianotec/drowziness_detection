%% Signal Splitting

% Base-Lenght
bLen = 7500;

if(basetime ~= 30)
    
    if (basetime == 15)
        
        % Signals are splitted in 15s sub-vectors
        dataSetW_15s = [dataSetW(:,1:(bLen/2));dataSetW(:,((bLen/2)+1):bLen)];
        dataSetS_15s = [dataSetS(:,1:(bLen/2));dataSetS(:,((bLen/2)+1):bLen)];
        
        dataSetW = dataSetW_15s;
        dataSetS = dataSetS_15s;
        
    else
        
        % Signals are splitted in 10s sub-vectors
        dataSetW_10s = [dataSetW(:,1:(bLen/3));dataSetW(:,((bLen/3)+1):(2*bLen/3));dataSetW(:,((2*bLen/3)+1):bLen)];
        dataSetS_10s = [dataSetS(:,1:(bLen/3));dataSetS(:,((bLen/3)+1):(2*bLen/3));dataSetS(:,((2*bLen/3)+1):bLen)];
        
        if(basetime ~= 10)
            
            % Signals are splitted in 5s sub-vectors
            dataSetW_5s = [dataSetW_10s(:,1:(bLen/6));dataSetW_10s(((bLen/6)+1):(bLen/3))];
            dataSetS_5s = [dataSetS_10s(:,1:(bLen/6));dataSetS_10s(((bLen/6)+1):(bLen/3))];
            
            if(basetime == 1)
                
                % Signals are splitted in 1s sub-vectors
                dataSetW_1s = [dataSetW_5s(:,1:(bLen/30));dataSetW_5s(:,((bLen/30)+1):(bLen/15));dataSetW_5s(:,((bLen/15)+1):(bLen/10));dataSetW_5s(:,((bLen/10)+1):(bLen/7.5));dataSetW_5s(:,((bLen/7.5)+1):(bLen/6))];
                dataSetS_1s = [dataSetS_5s(:,1:(bLen/30));dataSetS_5s(:,((bLen/30)+1):(bLen/15));dataSetS_5s(:,((bLen/15)+1):(bLen/10));dataSetS_5s(:,((bLen/10)+1):(bLen/7.5));dataSetS_5s(:,((bLen/7.5)+1):(bLen/6))];
                
                dataSetW = dataSetW_1s;
                dataSetS = dataSetS_1s;
                
            end
            
            if(basetime == 5)
                
                dataSetW = dataSetW_5s;
                dataSetS = dataSetS_5s;
                
            end
            
        else % if basetime = 10s
            
            dataSetW = dataSetW_10s;
            dataSetS = dataSetS_10s;
            
        end
        
    end
    
end

clearvars dataSetW_15s dataSetS_15s dataSetS_10s dataSetW_10s dataSetW_5s dataSetS_5s dataSetW_1s dataSetS_1s bLen

% -------------------------------------------------------------------------