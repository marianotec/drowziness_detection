%% Time Base GUI Input

% Time-Base values (sec): 1; 5; 10; 15; 30
ansI = inputdlg('Ingrese la base de tiempo (Valores aceptados: 1; 5; 10; 15; 30)');
basetime = str2num(ansI{1}); %#ok<ST2NM>

if( basetime ~= 1 && basetime ~= 5 && basetime ~= 10 && basetime ~= 15 && basetime ~= 30 )
    fprintf('Error, solo se aceptan las bases de tiempo 1, 5, 10, 15 ó 30...');
    fprintf('\n');
    return;
end

% Non used variables cleaning
clearvars ansI

% -------------------------------------------------------------------------